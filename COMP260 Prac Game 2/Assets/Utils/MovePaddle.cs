﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rb;
	private bool toggleMouseControl = true;

	void Start () {
		rb = GetComponent<Rigidbody>();
		rb.useGravity = false;

	}
	
	// Update is called once per frame
	public float maxSpeed = 5.0f;

	void Update() {
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (toggleMouseControl)
                toggleMouseControl = false;
            else
                toggleMouseControl = false;
        }
    }


	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

	public float speed = 20f;

    public Vector3 move;
    public Vector3 velocity;

    void FixedUpdate () {

        if (toggleMouseControl == false) {

            Vector2 direction;
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");

            // scale by the maxSpeed parameter
            Vector3 velocity = direction * maxSpeed;

            // move the object
            rb.MovePosition(transform.position + velocity * Time.deltaTime);

        } else {

		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rb.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rb.velocity = vel;
		}
	}

	/*
	public float force = 10f;

	void FixedUpdate () {
		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;

		rigidbody.AddForce(dir.normalized * force);
	}
	*/
}
